﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;

namespace ALE1PropositionHandling
{
    public partial class LogicalPropositionsHandling : Form
    {
        public LogicalPropositionsHandling()
        {
            InitializeComponent();
        }

        //form events
        private void btnCalculateStuff_Click(object sender, EventArgs e)
        {
            CalculateStuff(rtbInputFormula.Text);
        }

        private void LogicalPropositionsHandling_FormClosing(object sender, FormClosingEventArgs e)
        {
            //delete temporary files (tree text and png, etc.)
            if (File.Exists("./temp.png"))
                File.Delete("./temp.png");
            if (File.Exists("./temp.dot"))
                File.Delete("./temp.dot");
        }

        private void buttonGetRandomFormulaFromCollection_Click(object sender, EventArgs e)
        {
            HandleRandomFormula();
        }

        private void buttonEditList_Click(object sender, EventArgs e)
        {
            OpenListEditor();
        }

        private void buttonCalculateNextOwnFormula_Click(object sender, EventArgs e)
        {
            HandleNextOwnFormula();
        }

        //methods
        /// <summary>
        /// A counter to keep track of where we are in the self-made formulas.
        /// </summary>
        int ownFormulaCounter = 0;
        /// <summary>
        /// Calculate the next formula from the self-made formulas.
        /// </summary>
        private void HandleNextOwnFormula()
        {
            try
            {
                if (new FileInfo("./ownFormulas.json").Length > 2)
                {
                    string[] ownFormulas;
                    using (StreamReader sr = new StreamReader("./ownFormulas.json"))
                    {
                        ownFormulas = (string[])JsonConvert.DeserializeObject(sr.ReadToEnd(), typeof(string[]));
                    }
                    CalculateStuff(ownFormulas[ownFormulaCounter]); //get the next formula from the saved list
                    rtbInputFormula.Text = ownFormulas[ownFormulaCounter];
                    ownFormulaCounter++;
                }
                else
                {
                    throw new Exception("No formulas in the file. Please add formulas via the \"Edit list\" button.");
                }
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Please add formulas to the list.");
                OpenListEditor();
            }
            catch (IndexOutOfRangeException)
            {
                MessageBox.Show("No more formulas in the file. Restarting from 0.");
                ownFormulaCounter = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Something went wrong: \n"+ex.Message);
            }
        }

        /// <summary>
        /// Open the list editing form.
        /// </summary>
        private void OpenListEditor()
        {
            ownFormulaCounter = 0; //because changes were made, start from the beginning
            FormulaListEditor form = new FormulaListEditor();
            form.Show();
        }

        /// <summary>
        /// Calculates a random formula from a list of (approximately) 15000 auto-generated unique formulas using https://random-logic-generator.now.sh/.
        /// </summary>
        private void HandleRandomFormula()
        {
            List<string> formulas;

            using (StreamReader sr = new StreamReader("./randomFormulas.json"))
            {
                formulas = JsonConvert.DeserializeObject<List<string>>(sr.ReadToEnd());
            }

            Random rnd = new Random();
            rtbInputFormula.Text = formulas[rnd.Next(formulas.Count - 1)];

            CalculateStuff(rtbInputFormula.Text);
        }

        /// <summary>
        /// Calculates and shows the processed formula gotten from the form.
        /// </summary>
        private void CalculateStuff(string formulaString)
        {
            try
            {
                //create formula
                Formula formula = new Formula(formulaString);

                //binary tree
                BinaryTreeHandler binaryTreeHandler = new BinaryTreeHandler();
                binaryTreeHandler.GenerateTree(formula);
                pictureBoxBinaryTree.ImageLocation = "./temp.png";

                //show variables
                string formattedVariables = string.Empty;
                foreach (char c in formula.GetVariables())
                {
                    formattedVariables += $"{c}  ";
                }
                textBoxVariablesInFormula.Text = formattedVariables;

                //infix notation
                InfixNotationHandler infixHandler = new InfixNotationHandler();
                textBoxResultInfixNotation.Text = infixHandler.ConvertAsciiToInfix(formula);

                //truth table
                TruthTableHandler truthTableHandler = new TruthTableHandler();
                DataTable truthTable = truthTableHandler.BuildTruthTable(formula);
                
                PopulateListbox(listboxTruthTable, truthTable);

                //hash code
                List<int> results = new List<int>(truthTable.Rows.Count);
                foreach (DataRow row in truthTable.Rows)
                {
                    results.Add((int)char.GetNumericValue((char)row[truthTable.Columns.Count - 1]));
                }
                HashCodeHandler hashCodeHandler = new HashCodeHandler();
                textBoxHashCodeResult.Text = hashCodeHandler.ConvertBinaryToHex(results);

                //simplification
                SimplificationHandler simpleHandler = new SimplificationHandler();
                DataTable simplifiedTruthTable = simpleHandler.GetSimplifiedTruthTable(truthTable);

                PopulateListbox(listboxSimplifiedTruthTable, simplifiedTruthTable);

                //normalization
                textboxNormalizedFormula.Text = string.Empty;
                textboxNormalizedInfix.Text = string.Empty;

                if (!formula.IsContradiction)
                {
                    NormalizationHandler normalizer = new NormalizationHandler();

                    textboxNormalizedFormula.Text = normalizer.Normalize(truthTable);
                    Formula normalizedFormula = new Formula(textboxNormalizedFormula.Text);
                    textboxNormalizedInfix.Text = infixHandler.ConvertAsciiToInfix(normalizedFormula);


                    DataTable normalizedTruthTable = truthTableHandler.BuildTruthTable(normalizedFormula);
                    DataTable normalizedSimplifiedTruthTable = simpleHandler.GetSimplifiedTruthTable(normalizedTruthTable);

                    PopulateListbox(listboxNormalizedTruthTable, normalizedTruthTable);
                    PopulateListbox(listboxNormalizedSimplifiedTruthTable, normalizedSimplifiedTruthTable);
                }

                //nandify
                NandificationHandler nandifier = new NandificationHandler();
                textboxNandification.Text = nandifier.Nandify(formula);
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("No formula.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Something went wrong. \n" + ex.Message);
            }
        }

        /// <summary>
        /// Add the data from the DataTable to the ListBox.
        /// </summary>
        /// <param name="lb">The ListBox to populate.</param>
        /// <param name="table">The DataTable to take data from.</param>
        private void PopulateListbox(ListBox lb, DataTable table)
        {
            lb.DataSource = null;
            lb.Items.Clear();

            string columnNames = string.Empty;
            foreach (DataColumn column in table.Columns)
            {
                columnNames += column.ColumnName+" ";
            }
            lb.Items.Add(columnNames);
            lb.Items.Add(string.Empty);

            foreach (DataRow row in table.Rows)
            {
                string rowString = string.Empty;
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    rowString += row[i]+" ";
                }
                lb.Items.Add(rowString);
            }
        }

        /// <summary>
        /// Calculates all random formula from a list of (approximately) 15000 auto-generated unique formulas using https://random-logic-generator.now.sh/.
        /// </summary>
        private void HandleAllFormulas()
        {
            List<string> formulas;

            using (StreamReader sr = new StreamReader("./randomFormulas.json"))
            {
                formulas = JsonConvert.DeserializeObject<List<string>>(sr.ReadToEnd());
            }

            for (int i = 0; i < formulas.Count; i++)
            {
                rtbInputFormula.Text = formulas[i];
                Application.DoEvents();
                System.Threading.Thread.Sleep(100);
                Console.WriteLine($"{i}: {rtbInputFormula.Text}");
                CalculateStuff(rtbInputFormula.Text);
            }
        }
    }
}
