﻿namespace ALE1PropositionHandling
{
    partial class FormulaListEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonSave = new System.Windows.Forms.Button();
            this.richTextBoxFormulas = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // buttonSave
            // 
            this.buttonSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.buttonSave.Location = new System.Drawing.Point(12, 12);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(399, 65);
            this.buttonSave.TabIndex = 3;
            this.buttonSave.Text = "Save and quit";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // richTextBoxFormulas
            // 
            this.richTextBoxFormulas.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.richTextBoxFormulas.Location = new System.Drawing.Point(12, 83);
            this.richTextBoxFormulas.Name = "richTextBoxFormulas";
            this.richTextBoxFormulas.Size = new System.Drawing.Size(1296, 601);
            this.richTextBoxFormulas.TabIndex = 13;
            this.richTextBoxFormulas.Text = "";
            // 
            // FormulaListEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1320, 696);
            this.Controls.Add(this.richTextBoxFormulas);
            this.Controls.Add(this.buttonSave);
            this.Name = "FormulaListEditor";
            this.Text = "FormulaListEditor";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.RichTextBox richTextBoxFormulas;
    }
}