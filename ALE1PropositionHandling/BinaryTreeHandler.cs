﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace ALE1PropositionHandling
{
    public class BinaryTreeHandler
    {
        public void GenerateTree(Formula formula)
        {
            GenerateTextFile(formula);

            Process imageProcessingProcess = new Process();

            imageProcessingProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            imageProcessingProcess.StartInfo.FileName = Directory.GetCurrentDirectory() + "/graphviz/dot.exe";
            imageProcessingProcess.StartInfo.Arguments = "-Tpng -o\"temp.png\" temp.dot";
            imageProcessingProcess.Start();
            imageProcessingProcess.WaitForExit();
        }

        private void GenerateTextFile(Formula formula)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter("./temp.dot"))
                {

                    Node currentNode;
                    //startup
                    sw.WriteLine("graph logic{ node [ fontname = \"arial\"]");

                    //node definition in file
                    for (int i = 0; i < formula.Nodes.Count; i++)
                    {
                        currentNode = formula.Nodes[i];
                        sw.WriteLine($"node{i} [label=\"{currentNode.Character}\"]");
                    }

                    //node connections
                    for (int i = 0; i < formula.Nodes.Count; i++)
                    {
                        currentNode = formula.Nodes[i];
                        if (currentNode.Left != null)
                        {
                            sw.WriteLine($"node{i} -- node{formula.Nodes.IndexOf(currentNode.Left)}");
                        }
                        if (currentNode.Right != null)
                        {
                            sw.WriteLine($"node{i} -- node{formula.Nodes.IndexOf(currentNode.Right)}");
                        }
                    }

                    //closing
                    sw.WriteLine("}");
                }
            }
            catch
            {
                throw;
            }
        }
    }
}
