﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ALE1PropositionHandling
{
    public static class FormulaNodeOptions
    {
        public static char[] Operators = new char[]
        {
            '=',
            '>',
            '~',
            '|',
            '&',
        };
    }
}
