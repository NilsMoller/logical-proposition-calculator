﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ALE1PropositionHandling
{
    /// <summary>
    /// This holds the information and data structure of a parsed formula.
    /// </summary>
    public class Formula
    {
        public List<Node> Nodes { get; private set; }
        public Node RootNode { get; private set; }
        public string TrimmedOriginalString { get; private set; }
        public bool IsContradiction { get; set; }
        public bool IsTautology { get; set; }
        
        public Formula(string formula)
        {
            IsContradiction = false;
            IsTautology = false;
            Nodes = new List<Node>();
            RootNode = null;
            TrimmedOriginalString = formula.Replace(" ", string.Empty);

            RootNode = AssignNodes(TrimmedOriginalString.ToList());
        }

        /// <summary>
        /// Uses the formula given to generate nodes, set their left and right child nodes and adds that node to the list.
        /// </summary>
        /// <param name="formula"></param>
        /// <returns>Returns the root node.</returns>
        private Node AssignNodes(List<char> formula)
        {
            if (formula.Count == 0) //if we're out of characters just stop
            {
                return null;
            }
            else
            {
                if (FormulaNodeOptions.Operators.Contains(formula[0])) //if its an operator: assign the child nodes and add it to the nodes list
                {
                    char name = formula[0];
                    formula.RemoveAt(0);
                    Node left = AssignNodes(formula);
                    Node right = null;
                    if (name != '~')
                    {
                        right = AssignNodes(formula);
                    }
                    
                    Node newNode = new Node(name, left, right);

                    Nodes.Add(newNode);

                    return newNode;
                }
                else if (char.IsLetter(formula[0])) //if its a variable, return the new node. The child gets set in the operator check
                {
                    char c = formula[0];
                    formula.RemoveAt(0);

                    Node newNode = new Node(c);

                    Nodes.Add(newNode);

                    return newNode;
                }
                else //if its a comma or parenthesis, just go to the next character
                {
                    formula.RemoveAt(0);
                    return AssignNodes(formula);
                }
            }
        }

        /// <summary>
        /// Returns every unique variable that the formula contains.
        /// </summary>
        public char[] GetVariables()
        {
            List<char> chars = new List<char>();

            foreach (char character in TrimmedOriginalString)
            {
                if (char.IsLetter(character) && !chars.Contains(character))
                {
                    chars.Add(character);
                }
            }

            chars.Sort();
            
            return chars.ToArray();
        }
    }
}