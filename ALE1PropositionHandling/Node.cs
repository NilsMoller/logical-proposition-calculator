﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ALE1PropositionHandling
{
    public class Node
    {
        public char Character { get; private set; }

        public Node Left { get; set; }
        public Node Right { get; set; }

        public Node(char character)
        {
            Character = character;
            Left = null;
            Right = null;
        }

        public Node(char character, Node left, Node right)
        {
            Character = character;
            Left = left;
            Right = right;
        }
    }
}