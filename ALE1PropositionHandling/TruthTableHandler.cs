using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace ALE1PropositionHandling
{
    class TruthTableHandler
    {
        public DataTable BuildTruthTable(Formula formula)
        {
            DataTable table = new DataTable("TruthTable");
            char[] variables = formula.GetVariables();

            //create columns
            for (int i = 0; i < variables.Length; i++)
            {
                table.Columns.Add(variables[i].ToString(), typeof(char));
            }
            table.Columns.Add("Result", typeof(char));

            //create rows
            for (int i = 0; i < Math.Pow(2, variables.Length); i++)
            {
                string binary = Convert.ToString(i, 2); //row id in binary (doesnt prepend 0s)
                binary = binary.PadLeft(variables.Length, '0'); //pad to fill in the rest of the columns
                DataRow row = table.NewRow();

                for (int j = 0; j < table.Columns.Count - 1; j++) //populate current row
                {
                    row[table.Columns[j]] = binary[j];
                }
                table.Rows.Add(row);

                //create result
                row.SetField("Result", GetResult(row, formula.RootNode).ToString());
            }

            List<int> results = new List<int>(table.Rows.Count);
            foreach (DataRow row in table.Rows)
            {
                results.Add((int)char.GetNumericValue((char)row[table.Columns.Count - 1]));
            }
            if (!results.Contains(1)) //return original table because we can't simplify false results
            {
                formula.IsContradiction = true;
                formula.IsTautology = false;
            }
            else if (!results.Contains(0)) //in case of all 1s the simplification table has 1 row with all *s and 1 for a result
            {
                formula.IsContradiction = false;
                formula.IsTautology = true;
            }

            return table;
        }

        private int GetResult(DataRow row, Node currentNode)
        {
            //define behavior for every situation.
            if (char.IsLetter(currentNode.Character)) //if its a letter, return the value given to it in the table
            {
                return (int)char.GetNumericValue((char)row[currentNode.Character.ToString()]);
            }
            else
            {
                if (currentNode.Character == '~') //negation behavior: true if left is false
                {
                    return GetNegationResult(row, currentNode);
                }
                else if (currentNode.Character == '>') //implication behavior: true if left is false or right is true, or both left is false and right is true
                {
                    return GetImplicationResult(row, currentNode);
                }
                else if (currentNode.Character == '=') //biimplication behavior: true if left and right has same value
                {
                    return GetBiimplicationResult(row, currentNode);
                }
                else if (currentNode.Character == '&') //conjuction behavior: only true if both left and right are true
                {
                    return GetConjuctionResult(row, currentNode);
                }
                else// if (currentNode.Character == '|') //disjunction behavior: true if left, right, or both are true
                {
                    return GetDisjunctionResult(row, currentNode);
                }
            }
            
        }

        //not ~
        private int GetNegationResult(DataRow row, Node currentNode)
        {
            int leftValue;
            if (char.IsLetter(currentNode.Left.Character)) //if its a letter, grab the value and return the negated one
            {
                leftValue = (int)char.GetNumericValue((char)row[currentNode.Left.Character.ToString()]);
                if (leftValue == 0)
                {
                    return 1;
                }
                else //value is 1
                {
                    return 0;
                }
            }
            else //if its an operator, return the negated value of the result of that operator
            {
                leftValue = GetResult(row, currentNode.Left);
                if (leftValue == 0)
                {
                    return 1;
                }
                else //value is 1
                {
                    return 0;
                }
            }
        }

        //not left or right >
        private int GetImplicationResult(DataRow row, Node currentNode)
        {
            int leftValue;
            int rightValue;

            //get the left and right value as either 0 or 1
            if (char.IsLetter(currentNode.Left.Character))
            {
                leftValue = (int)char.GetNumericValue((char)row[currentNode.Left.Character.ToString()]);
            }
            else
            {
                leftValue = GetResult(row, currentNode.Left);
            }
            if (char.IsLetter(currentNode.Right.Character))
            {
                rightValue = (int)char.GetNumericValue((char)row[currentNode.Right.Character.ToString()]);
            }
            else
            {
                rightValue = GetResult(row, currentNode.Right);
            }

            //use the left and right values to determine what to return
            if (leftValue == 0 || rightValue == 1)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        //the same =
        private int GetBiimplicationResult(DataRow row, Node currentNode)
        {
            int leftValue;
            int rightValue;

            //get the left and right value as either 0 or 1
            if (char.IsLetter(currentNode.Left.Character))
            {
                leftValue = (int)char.GetNumericValue((char)row[currentNode.Left.Character.ToString()]);
            }
            else
            {
                leftValue = GetResult(row, currentNode.Left);
            }
            if (char.IsLetter(currentNode.Right.Character))
            {
                rightValue = (int)char.GetNumericValue((char)row[currentNode.Right.Character.ToString()]);
            }
            else
            {
                rightValue = GetResult(row, currentNode.Right);
            }

            //return the result
            if (leftValue == rightValue)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        //and &
        private int GetConjuctionResult(DataRow row, Node currentNode)
        {
            int leftValue;
            int rightValue;

            //get the left and right value as either 0 or 1
            if (char.IsLetter(currentNode.Left.Character))
            {
                leftValue = (int)char.GetNumericValue((char)row[currentNode.Left.Character.ToString()]);
            }
            else
            {
                leftValue = GetResult(row, currentNode.Left);
            }
            if (char.IsLetter(currentNode.Right.Character))
            {
                rightValue = (int)char.GetNumericValue((char)row[currentNode.Right.Character.ToString()]);
            }
            else
            {
                rightValue = GetResult(row, currentNode.Right);
            }

            //return the result
            if (leftValue == 1 && rightValue == 1)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        //or |
        private int GetDisjunctionResult(DataRow row, Node currentNode)
        {
            int leftValue;
            int rightValue;

            //get the left and right value as either 0 or 1
            if (char.IsLetter(currentNode.Left.Character))
            {
                leftValue = (int)char.GetNumericValue((char)row[currentNode.Left.Character.ToString()]);
            }
            else
            {
                leftValue = GetResult(row, currentNode.Left);
            }
            if (char.IsLetter(currentNode.Right.Character))
            {
                rightValue = (int)char.GetNumericValue((char)row[currentNode.Right.Character.ToString()]);
            }
            else
            {
                rightValue = GetResult(row, currentNode.Right);
            }

            //return the result
            if (leftValue == 1 || rightValue == 1)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
